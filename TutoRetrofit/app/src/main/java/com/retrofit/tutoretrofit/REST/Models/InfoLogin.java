package com.retrofit.tutoretrofit.REST.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neko on 19/11/15.
 */
public class InfoLogin {

    @SerializedName("Data")
    private String data;

    @SerializedName("LoginType")
    private String loginType;

    @SerializedName("PIN")
    private String pin;

    @SerializedName("AppVersion")
    private String appVersion;

    @SerializedName("Device")
    private String device;

    @SerializedName("ForceClose")
    private boolean forceClose;

    @SerializedName("Language")
    private String languaje;

    public String getLanguaje() {
        return languaje;
    }

    public void setLanguaje(String languaje) {
        this.languaje = languaje;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public boolean isForceClose() {
        return forceClose;
    }

    public void setForceClose(boolean forceClose) {
        this.forceClose = forceClose;
    }
}
