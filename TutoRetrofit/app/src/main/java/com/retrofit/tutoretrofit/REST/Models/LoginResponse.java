package com.retrofit.tutoretrofit.REST.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neko on 19/11/15.
 *
 * *******************************************************
 * ESTE ES EL JSON DE RESPUESTA PARA EL SERVICIO REST
 * *******************************************************
 *
 * "LoginResult": {
 *      "ErrorMessage": "Empty if correct or if the client doesn't need message",
 *      "ResponseCode": (int),
 *      "SessionId": (long, Id of the session, needed to check the status of the session),
 *      "UserId": (long, id of the user)
 * }
 */
public class LoginResponse {
    @SerializedName("LoginResult")
    private loginResult loginResult;

    public LoginResponse.loginResult getLoginResult() {
        return loginResult;
    }

    public void setLoginResult(LoginResponse.loginResult loginResult) {
        this.loginResult = loginResult;
    }

    public class loginResult{
        @SerializedName("ErrorMessage")
        private String errorMessage;

        @SerializedName("ResponseCode")
        private int responseCode;

        @SerializedName("SessionId")
        private Long sessionId;

        @SerializedName("UserId")
        private Long userId;

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public Long getSessionId() {
            return sessionId;
        }

        public void setSessionId(Long sessionId) {
            this.sessionId = sessionId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }
    }
}
