package com.retrofit.tutoretrofit.REST.WebServices;

import com.retrofit.tutoretrofit.REST.Models.LoginRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by neko on 19/11/15.
 *
 * SErvicio encargado de realizar la peticion al servidor
 */
public interface LoginServiceCallback {
    @POST("/Login") //aqui debera ir exactamente lo que la url indique despues de '/'.. https://URL/login.php, login, login.net... etc
    void getResponse(@Body LoginRequest request, Callback<LoginResponse> callback);
}
