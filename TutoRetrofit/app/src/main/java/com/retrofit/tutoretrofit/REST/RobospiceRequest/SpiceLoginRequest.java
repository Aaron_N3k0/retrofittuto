package com.retrofit.tutoretrofit.REST.RobospiceRequest;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginResponse;
import com.retrofit.tutoretrofit.REST.WebServices.LoginServiceRobospice;

/**
 * Created by neko on 04/12/15.
 */
public class SpiceLoginRequest extends RetrofitSpiceRequest<LoginResponse, LoginServiceRobospice> {

    private LoginRequest request;

    public SpiceLoginRequest(LoginRequest request) {
        super(LoginResponse.class, LoginServiceRobospice.class);
        this.request = request;
    }

    @Override
    public LoginResponse loadDataFromNetwork() throws Exception {
        return getService().getResponse(request);
    }
}
