package com.retrofit.tutoretrofit.REST;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by cbriseno on 13/04/15.
 */
public abstract class SpiceHelper<CLASS_RESPONSE> {

    private SpiceManager spiceManager;
    private String cacheKey;
    private boolean isRequestPending;
    private Class responseClass;

    public SpiceHelper(SpiceManager spiceManager, String cacheKey, Class responseClass) {
        this.cacheKey = cacheKey;
        this.spiceManager = spiceManager;
        this.responseClass = responseClass;
        isRequestPending = false;
    }

    private void clearCacheRequest() {
        try {
            spiceManager.removeAllDataFromCache();
            Future<?> future = spiceManager.removeAllDataFromCache();
            if (future != null) {
                future.get();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    protected void onFail(SpiceException exception) {
        clearCacheRequest();
        isRequestPending = false;
    }

    protected void onSuccess(CLASS_RESPONSE response) {
        clearCacheRequest();
        isRequestPending = false;
    }

    public void clearCache() {
        clearCacheRequest();
    }

    public void executeRequest(SpiceRequest<CLASS_RESPONSE> request) {
        spiceManager.execute(request, cacheKey, DurationInMillis.ONE_HOUR, new SpiceRequestListener());
    }

    public void executePendingRequest() {
        isRequestPending = true;
        spiceManager.addListenerIfPending(responseClass, cacheKey, new SpicePendingRequestListener());
    }

    public boolean isRequestPending() {
        return isRequestPending;
    }

    private class SpiceRequestListener implements RequestListener<CLASS_RESPONSE> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

    private class SpicePendingRequestListener implements PendingRequestListener<CLASS_RESPONSE> {

        @Override
        public void onRequestNotFound() {
            if (isRequestPending) {
                spiceManager.getFromCache(responseClass, cacheKey, DurationInMillis.ONE_DAY, new SpiceRequestListener());
            }
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

}
