package com.retrofit.tutoretrofit.REST;

import android.app.Application;
import android.util.Log;

import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.retrofit.tutoretrofit.REST.Models.LoginRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginResponse;
import com.retrofit.tutoretrofit.REST.Persisters.GsonObjectPersister;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit.RestAdapter;
import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;

/**
 * Created by neko on 05/12/15.
 * @see RetrofitGsonSpiceService
 * @link {REST PACKAGE}
 */
public class RoboSpiceService extends RetrofitGsonSpiceService {

    public static final String BASE_URL = "https://testing.secapp.co/WebServices/APIv2.svc";

    @Override
    public void onCreate() {
        super.onCreate();

        //LOGIN
        addRetrofitInterface(LoginRequest.class);
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(new RestAdapter.Log() {

            @Override
            public void log(String message) {
                Log.i("REST ADAPTER: ", message);
            }
        });
        builder.setClient(new UrlConnectionClient() {
            @Override
            protected HttpURLConnection openConnection(Request request) throws IOException {
                HttpURLConnection connection = super.openConnection(request);
                connection.setConnectTimeout(10 * 1000); //10 seg
                connection.setReadTimeout(10 * 1000); //10 seg
                return connection;
            }
        });
        return builder;
    }

    /**
     * Metodo usado por RoboSpice para almacenar en cache las peticiones hechas al servidor
     * @see CacheManager
     * */
    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager manager = new CacheManager();

        //LOGIN PERSISTER
        GsonObjectPersister<LoginResponse> persisterLogin =
                new GsonObjectPersister<>(application, LoginResponse.class);
        manager.addPersister(persisterLogin);

        return manager;
    }


}
