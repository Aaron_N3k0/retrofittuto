package com.retrofit.tutoretrofit;

import android.app.Activity;

import com.octo.android.robospice.SpiceManager;
import com.retrofit.tutoretrofit.REST.RoboSpiceService;

/**
 * Created by secapp on 04/12/2015.
 */
public abstract class BaseActivity extends Activity{

    //start RobospiceService
    protected SpiceManager spiceManager = new SpiceManager(RoboSpiceService.class);

    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(this);
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager(){
        return spiceManager;
    }
}