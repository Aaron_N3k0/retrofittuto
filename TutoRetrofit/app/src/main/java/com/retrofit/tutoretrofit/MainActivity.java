package com.retrofit.tutoretrofit;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.retrofit.tutoretrofit.REST.Models.InfoLogin;
import com.retrofit.tutoretrofit.REST.Models.LoginRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginResponse;
import com.retrofit.tutoretrofit.REST.RoboSpiceService;
import com.retrofit.tutoretrofit.REST.RobospiceRequest.SpiceLoginRequest;
import com.retrofit.tutoretrofit.REST.SpiceHelper;
import com.retrofit.tutoretrofit.REST.WebServices.LoginServiceCallback;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity {

    private Button btn2;
    private Button btn1;

    //Link global
    final String BASE_URL = "https://testing.secapp.co/WebServices/APIv2.svc";
    private TextView tvBody;

    //Variables Robospice Login
    private static final String LOGIN_CACHE = "login_cache";
    private SpiceHelper<LoginResponse> mSpiceLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        tvBody = (TextView) findViewById(R.id.tvBody);

        initRobspiceComponents();

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Llamo el metodo que consumira el servicio REST de login
                getSingleRetrofitResponse();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRobospiceResponse();
            }
        });

    }

    public void initRobspiceComponents(){
        //==========================================================================
        //*                 SPICE MANAGER - LOGIN KEY_CACHE_LOGIN,
        //==========================================================================
        mSpiceLogin = new SpiceHelper<LoginResponse>(getSpiceManager(), LOGIN_CACHE, LoginResponse.class) {
            @Override
            protected void onSuccess(LoginResponse loginResponse) {
                super.onSuccess(loginResponse);

                //LoginResponse ya viene convertido de JSON a Objeto,
                // por eso se puede usarn los getters
                Toast.makeText(MainActivity.this,
                        "RESPONSE CODE: "
                                +loginResponse.getLoginResult().getResponseCode()
                                + "\nSESSION ID: "+loginResponse.getLoginResult().getSessionId()
                                + "\nUSER ID: "+loginResponse.getLoginResult().getUserId(),
                        Toast.LENGTH_SHORT).show();

                //Oh se pueden volver a convertir a JSON, usndo la librearia Gson
                tvBody.setText("Login data: \n\n" + new Gson().toJson(loginResponse));

            }

            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Toast.makeText(MainActivity.this, "Ups, ocurrio un error", Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();

        //Compruebo si hay datos en cache para continuar desde donde se quedo el Request
        if(mSpiceLogin != null && mSpiceLogin.isRequestPending()){
            mSpiceLogin.executePendingRequest();
        }
    }

    /**Se esta usando una peticion Asyncrona de Retrofit (cuando puedas investiga, synchronous and asynchronous)
     * https://futurestud.io/blog/retrofit-synchronous-and-asynchronous-requests/
     * */
    private void getSingleRetrofitResponse(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {

                //SE inicia el Adaptador de Retrofit con la URL
                RestAdapter retrofit = new RestAdapter.Builder()
                        .setEndpoint(BASE_URL).build();

                //SE crea una instancia del servicio POST Login
                LoginServiceCallback service = retrofit.create(LoginServiceCallback.class);

                //=========================================================================
                //                     Haciendo uso de los modelos
                //=========================================================================

                //Se hace una instancia del modelo de peticion que se mandara al servidor
                InfoLogin infoLogin = new InfoLogin();
                infoLogin.setData("garabato11@gmail.com");
                infoLogin.setLoginType("2");
                infoLogin.setPin("1234");
                infoLogin.setAppVersion("3");
                infoLogin.setDevice("10");
                infoLogin.setLanguaje("1");
                infoLogin.setForceClose(true);

                //Se setea la info del objeto InfoLogin
                LoginRequest request = new LoginRequest();
                request.setInfoLogin(infoLogin);

                //=========================================================================
                //                Fin de Seteo de informacion de los modelos
                //=========================================================================

                //Se inicia la peticion del servicio Retrofit y el callback
                service.getResponse(request, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginResponse, Response response) {

                        //LoginResponse ya viene convertido de JSON a Objeto,
                        // por eso se puede usarn los getters
                        Toast.makeText(MainActivity.this,
                                "RESPONSE CODE: "
                                        +loginResponse.getLoginResult().getResponseCode()
                                        + "\nSESSION ID: "+loginResponse.getLoginResult().getSessionId()
                                        + "\nUSER ID: "+loginResponse.getLoginResult().getUserId(),
                                Toast.LENGTH_SHORT).show();

                        //Oh se pueden volver a convertir a JSON, usndo la librearia Gson
                        tvBody.setText("Login data: \n\n" + new Gson().toJson(loginResponse));

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(MainActivity.this, "Ups, ocurrio un error", Toast.LENGTH_SHORT).show();
                    }
                });

                return null;
            }
        }.execute();
    }

    private void getRobospiceResponse(){
        //Se hace una instancia del modelo de peticion que se mandara al servidor
        InfoLogin infoLogin = new InfoLogin();
        infoLogin.setData("garabato11@gmail.com");
        infoLogin.setLoginType("2");
        infoLogin.setPin("1234");
        infoLogin.setAppVersion("3");
        infoLogin.setDevice("10");
        infoLogin.setLanguaje("1");
        infoLogin.setForceClose(true);

        //Se setea la info del objeto InfoLogin
        LoginRequest request = new LoginRequest();
        request.setInfoLogin(infoLogin);

        //Se setean los datos del listener
        SpiceLoginRequest requestListener = new SpiceLoginRequest(request);

        //se Iniciar el listener (de tipo SpiceHelper) robospice
        mSpiceLogin.executeRequest(requestListener);
    }

}
