package com.retrofit.tutoretrofit.REST.WebServices;

import com.retrofit.tutoretrofit.REST.Models.LoginRequest;
import com.retrofit.tutoretrofit.REST.Models.LoginResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by neko on 04/12/15.
 *
 * SErvicio encargado de realizar la peticion al servidor
 */
public interface LoginServiceRobospice {
    @POST("/Login")
    LoginResponse getResponse(@Body LoginRequest request); //ahora no es VOID, sino que devuelve un objeto de respuesta
}
