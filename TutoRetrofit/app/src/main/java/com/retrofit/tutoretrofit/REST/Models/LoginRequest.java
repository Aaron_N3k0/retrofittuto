package com.retrofit.tutoretrofit.REST.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by neko on 19/11/15.
 *
 * En base a la estructura JSON request (peticion)
 * que se encuenta en el archivo de texto "LEEME"
 *
 * Se Genera una clase objeto LoginRequest, con sus respectivos parametros
 *
 * @SerializeName es una propiedad de la librearia GSON,
 * que nos permite convertir objetos a JSON y viceversa.
 * Asi puedo decirle que (Login) se refiere a un objeto "InfoLogin" al convertirlo en JSON
 *
 * {
    "InfoLogin": { //Objeto InfoLogin del JSON
        "Data": "",
        "LoginType": "",
        "PIN": "",
        "AppVersion": "",
        "Device": "",
        "ForceClose": true/false,
        "Language": ""
    }
* }
 */
public class LoginRequest {

    // Objeto InfoLogin del JSON, sin importar el nombre de la variable de abajo (login, en este caso),
    // siempre se convertira con el nombre "InfoLogin"
    @SerializedName("InfoLogin")
    private InfoLogin Login;

    //Getters y setters de InfoLogin
    public InfoLogin getInfoLogin() {
        return Login;
    }
    public void setInfoLogin(InfoLogin infoLogin) {
        this.Login = infoLogin;
    }

    //========================================================================
    // Con esta clase de abajo, se representa el Objeto "InfoLogin" del JSON,
    // pero yo lo saque en una clase independiente solo por comodidad
    //========================================================================

//    public class InfoLogin {
//
//        @SerializedName("Data")
//        private String data;
//
//        @SerializedName("LoginType")
//        private String loginType;
//
//        @SerializedName("PIN")
//        private String pin;
//
//        @SerializedName("AppVersion")
//        private String appVersion;
//
//        @SerializedName("Device")
//        private String device;
//
//        @SerializedName("ForceClose")
//        private boolean forceClose;
//
//        @SerializedName("Language")
//        private String languaje;
//
//        public String getLanguaje() {
//            return languaje;
//        }
//
//        public void setLanguaje(String languaje) {
//            this.languaje = languaje;
//        }
//
//        public String getData() {
//            return data;
//        }
//
//        public void setData(String data) {
//            this.data = data;
//        }
//
//        public String getLoginType() {
//            return loginType;
//        }
//
//        public void setLoginType(String loginType) {
//            this.loginType = loginType;
//        }
//
//        public String getPin() {
//            return pin;
//        }
//
//        public void setPin(String pin) {
//            this.pin = pin;
//        }
//
//        public String getAppVersion() {
//            return appVersion;
//        }
//
//        public void setAppVersion(String appVersion) {
//            this.appVersion = appVersion;
//        }
//
//        public String getDevice() {
//            return device;
//        }
//
//        public void setDevice(String device) {
//            this.device = device;
//        }
//
//        public boolean isForceClose() {
//            return forceClose;
//        }
//
//        public void setForceClose(boolean forceClose) {
//            this.forceClose = forceClose;
//        }
//    }

}
